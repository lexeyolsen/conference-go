import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    content = json.loads(body)
    name = content["presenter_name"]
    email = content["presenter_email"]
    title = content["title"]

    send_mail(
        "Your presentation has been accepted",
        f"{name}, your presentation: {title}, has been accepted",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    content = json.loads(body)
    name = content["presenter_name"]
    email = content["presenter_email"]
    title = content["title"]

    send_mail(
        "Your presentation has been rejected",
        f"{name}, your presentation: {title}, has been rejected",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )


def send_to_queue():
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    approval_channel = connection.channel()
    rejection_channel = connection.channel()
    approval_channel.queue_declare(queue="presentation_approvals")
    rejection_channel.queue_declare(queue="presentation_rejections")
    approval_channel.basic_consume(
        queue="presentation_approvals",
        on_message_callback=process_approval,
        auto_ack=True,
    )
    rejection_channel.basic_consume(
        queue="presentation_rejections",
        on_message_callback=process_rejection,
        auto_ack=True,
    )
    print("waiting for message")
    approval_channel.start_consuming()
    rejection_channel.start_consuming()


if __name__ == "__main__":
    try:
        send_to_queue()
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit()
        except SystemExit:
            os._exit(0)
