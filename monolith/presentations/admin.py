from django.contrib import admin

from .models import Presentation, Status


@admin.register(Presentation)
class PresentationAdmin(admin.ModelAdmin):
    list_display = ("id", "title")


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    pass
