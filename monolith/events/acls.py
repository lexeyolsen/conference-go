# from tkinter import Y
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests
from .api_views import Location


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    x = requests.get(url, params=params, headers=headers)
    content = json.loads(x.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except Location.picture_url.DoesNotExist:
        return {"picture_url": None}


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # url = "http://api.openweathermap.org/geo/1.0/direct?q=%7Bcity name},{state code},{country code}&limit={limit}&appid={API key}"

    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    x = requests.get(url, params=params)
    y = json.loads(x.content)
    L1 = y[0]["lat"]
    L2 = y[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": L1,
        "lon": L2,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    x = requests.get(url, params=params)
    content = json.loads(x.content)

    try:
        return {"weather": content["weather"], "temp": content["main"]["temp"]}
    except:
        return {"weather": None}


# def get_flight_data(city, state):


#   url = "http://api.openweathermap.org/geo/1.0/direct?q=%7Bcity name},{state code},{country code}&limit={limit}&appid={API key}
